package org.sadtech.bot.autoresponder.domain.unit;

public enum TypeUnit {

    TEXT, SAVE, PROCESSING, TIMER, CHECK, VALIDITY, ACCOUNT

}

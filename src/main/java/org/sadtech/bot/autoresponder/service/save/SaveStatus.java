package org.sadtech.bot.autoresponder.service.save;

public enum SaveStatus {

    INIT, SAVE, FINISH, FULL

}
